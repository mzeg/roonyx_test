package ru.roonyx.mzeg.testtask.repository;

import org.springframework.data.repository.CrudRepository;
import ru.roonyx.mzeg.testtask.model.TeacherSubject;

public interface TeacherSubjectRepository extends CrudRepository<TeacherSubject, Long> {
}
