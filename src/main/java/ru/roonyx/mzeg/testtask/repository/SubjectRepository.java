package ru.roonyx.mzeg.testtask.repository;

import org.springframework.data.repository.CrudRepository;
import ru.roonyx.mzeg.testtask.model.Subject;

public interface SubjectRepository extends CrudRepository<Subject, Long> {
}
