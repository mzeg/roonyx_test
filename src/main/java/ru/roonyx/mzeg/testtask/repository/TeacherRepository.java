package ru.roonyx.mzeg.testtask.repository;

import org.springframework.data.repository.CrudRepository;
import ru.roonyx.mzeg.testtask.model.Teacher;

public interface TeacherRepository extends CrudRepository<Teacher, Long> {
}
