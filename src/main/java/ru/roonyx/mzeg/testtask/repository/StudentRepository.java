package ru.roonyx.mzeg.testtask.repository;

import org.springframework.data.repository.CrudRepository;
import ru.roonyx.mzeg.testtask.model.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {
}
