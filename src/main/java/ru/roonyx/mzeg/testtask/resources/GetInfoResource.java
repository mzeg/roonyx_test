package ru.roonyx.mzeg.testtask.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.roonyx.mzeg.testtask.dto.ResultDTO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@BasePathAwareController
@RepositoryRestResource
@RequestMapping("getInfo")
public class GetInfoResource {
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<ResultDTO> get(
            @RequestParam(value = "student", required = false) List<String> student,
            @RequestParam(value = "subject", required = false) List<String> subject,
            @RequestParam(value = "teacher", required = false) List<String> teacher
    ) {
        String sql =
                "SELECT st.name AS student_name, t.name AS teacher_name, su.name AS subject_name " +
                        "FROM student_teacher_subjects sts " +
                        "JOIN student st ON st.id=sts.student_id " +
                        "JOIN teacher_subject ts ON ts.id=sts.teacher_subjects_id " +
                        "JOIN teacher t ON t.id=ts.teacher_id " +
                        "JOIN subject su ON su.id=ts.subject_id ";

        boolean hasWhere = false;

        if (student != null) {
            sql = sql + (hasWhere ? " AND " : " WHERE ") + "st.name IN (:student)";
            hasWhere = true;
        }

        if (subject != null) {
            sql = sql + (hasWhere ? " AND " : " WHERE ") + "su.name IN (:subject)";
            ;
            hasWhere = true;
        }

        if (teacher != null) {
            sql = sql + (hasWhere ? " AND " : " WHERE ") + "t.name IN (:teacher)";
        }

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("student", student)
                .addValue("subject", subject)
                .addValue("teacher", teacher);

        return namedParameterJdbcTemplate.
                query(
                        sql,
                        parameters,
                        new RowMapper<ResultDTO>() {
                            @Override
                            public ResultDTO mapRow(ResultSet resultSet, int i) throws SQLException {
                                return ResultDTO.builder()
                                        .studentName(resultSet.getString("student_name"))
                                        .subjectName(resultSet.getString("subject_name"))
                                        .teacherName(resultSet.getString("teacher_name"))
                                        .build();
                            }
                        }
                );
    }
}