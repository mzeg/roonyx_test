package ru.roonyx.mzeg.testtask.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.roonyx.mzeg.testtask.model.Student;
import ru.roonyx.mzeg.testtask.model.Subject;
import ru.roonyx.mzeg.testtask.model.Teacher;
import ru.roonyx.mzeg.testtask.model.TeacherSubject;
import ru.roonyx.mzeg.testtask.repository.StudentRepository;
import ru.roonyx.mzeg.testtask.repository.SubjectRepository;
import ru.roonyx.mzeg.testtask.repository.TeacherRepository;
import ru.roonyx.mzeg.testtask.repository.TeacherSubjectRepository;

import java.util.Arrays;

@BasePathAwareController
@RepositoryRestResource
@RequestMapping("generateTestData")
public class GenerateTestDateResource {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private SubjectRepository subjectRepository;
    @Autowired
    private TeacherSubjectRepository teacherSubjectRepository;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void post() {
        // Subjects
        Subject subjectMath = new Subject();
        subjectMath.setName("Математика");
        subjectRepository.save(subjectMath);

        Subject subjectBio = new Subject();
        subjectBio.setName("Биология");
        subjectRepository.save(subjectBio);

        Subject subjectHist = new Subject();
        subjectHist.setName("История");
        subjectRepository.save(subjectHist);

        // Teachers
        Teacher teacherIvanov = new Teacher();
        teacherIvanov.setName("Иванов");
        teacherRepository.save(teacherIvanov);

        Teacher teacherPetrov = new Teacher();
        teacherPetrov.setName("Петров");
        teacherRepository.save(teacherPetrov);

        Teacher teacherSidorov = new Teacher();
        teacherSidorov.setName("Сидоров");
        teacherRepository.save(teacherSidorov);

        // TeacherSubjects
        TeacherSubject mathIvanov=new TeacherSubject();
        mathIvanov.setSubject(subjectMath);
        mathIvanov.setTeacher(teacherIvanov);
        teacherSubjectRepository.save(mathIvanov);


        TeacherSubject bioPetrov=new TeacherSubject();
        bioPetrov.setSubject(subjectBio);
        bioPetrov.setTeacher(teacherPetrov);
        teacherSubjectRepository.save(bioPetrov);

        TeacherSubject bioSidorov = new TeacherSubject();
        bioSidorov.setSubject(subjectBio);
        bioSidorov.setTeacher(teacherSidorov);
        teacherSubjectRepository.save(bioSidorov);

        TeacherSubject histSidorov = new TeacherSubject();
        histSidorov.setSubject(subjectHist);
        histSidorov.setTeacher(teacherSidorov);
        teacherSubjectRepository.save(histSidorov);

        // Students
        Student studentAlex = new Student();
        studentAlex.setName("Александр");
        studentAlex.setTeacherSubjects(Arrays.asList(new TeacherSubject[]{mathIvanov, bioPetrov}));
        studentRepository.save(studentAlex);

        Student studentEgor = new Student();
        studentEgor.setName("Егор");
        studentEgor.setTeacherSubjects(Arrays.asList(new TeacherSubject[]{bioSidorov, histSidorov}));
        studentRepository.save(studentEgor);
    }
}
