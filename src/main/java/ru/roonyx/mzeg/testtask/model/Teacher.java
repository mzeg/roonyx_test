package ru.roonyx.mzeg.testtask.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Teacher {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
}
