package ru.roonyx.mzeg.testtask.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultDTO {
    private String studentName;
    private String teacherName;
    private String subjectName;
}
